﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EcommerceMat.Startup))]
namespace EcommerceMat
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
