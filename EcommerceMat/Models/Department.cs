﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EcommerceMat.Models
{
    public class Department
    { 
        [Key]
        public int DepartmentId { get; set; }
        [Required(ErrorMessage ="El campo {0} es Requerido)")]
        [MaxLength(50, ErrorMessage =("El campo {0} debe ser menos a {1} caracteres de longitud"))]
        public string Name { get; set; }


        public virtual ICollection<City> Cities { get; set; }

    }
}