﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EcommerceMat.Models
{
    public class City
    {
        [Key]
        public int CityId { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Este campo Es requerido")]
        public int DepartmentId { get; set; }

        public virtual Department Department { get; set; }

    }
}