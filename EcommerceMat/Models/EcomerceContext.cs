﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EcommerceMat.Models
{
    public class EcomerceContext : DbContext
    {
        public EcomerceContext() : base("DefaultConnection")
        {

        }

        public System.Data.Entity.DbSet<EcommerceMat.Models.Department> Departments { get; set; }

        public System.Data.Entity.DbSet<EcommerceMat.Models.City> Cities { get; set; }
    }
}